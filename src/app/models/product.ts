export class Product {
	constructor(
		public id: string,
		public name: string,
		public brand: string,
		public brandImage: string,
		public kind: string,
		public kindImage: string,
		public price: number
	) { }
}
