export class Filters {
	constructor(
		public brand: string,
		public name: string,
		public kind: string,
		public price: string
	) { }
}
