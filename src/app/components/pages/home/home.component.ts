import { Component, OnInit } from '@angular/core';
import { AppInfoService } from '../../../services/app-info.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	constructor(
		private appInfo: AppInfoService
	) {
		this.changeIsHome();
	}

	ngOnInit() {

	}

	changeIsHome() {
		this.appInfo.changeisHome(true);
	}


}
