import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../../../models/product';
import { ProductsService } from '../../../services/products.service';
import { AppInfoService } from '../../../services/app-info.service';


@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
	public filtersAreCollapsed = true;
	public loading = true;
	public responseService: any;
	public productList: Array<Product>;
	public totalProducts = 0;
	public totalPages = 0;
	public productPerPage = 5;
	public page = 0;
	public filters: any;
	public sort = 'brand';
	public order = 'asc';

	constructor(
		private appInfo: AppInfoService,
		private productsService: ProductsService,
		private route: ActivatedRoute
	) {
		this.appInfo.filterState.subscribe(result => this.filters = result);
		this.retrieveProducts();
		this.changeIsHome();
	}

	ngOnInit() {

	}

	onNext(): void {
		this.loading = true;
		this.page++;
		this.retrieveProducts();
	}

	onPrev(): void {
		this.loading = true;
		this.page--;
		this.retrieveProducts();
	}

	eventFromChild(change) {
		this.loading = true;
		this.page = 0;
		if (change) {
			this.retrieveProducts();
		}
	}

	retrieveProducts() {
		setTimeout(() => {
			this.responseService = this.productsService.getProducts(this.filters, this.productPerPage, this.page, this.sort, this.order);
			this.productList = this.responseService.products;
			this.totalProducts = this.responseService.total;
			this.totalPages = this.responseService.pages;
			this.loading = false;
		}, 600);

	}

	changeIsHome() {
		this.appInfo.changeisHome(false);
	}

	sortBy(sort, order) {
		this.sort = sort;
		this.order = order;
		this.retrieveProducts();
	}



}
