import { Component, Input, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AppInfoService } from '../../../services/app-info.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	@Input() state;
	public closeResult: string;
	public isHome: boolean;
	public hasFav = false;

	constructor(
		private modalService: NgbModal,
		private appInfo: AppInfoService
	) {

	}

	ngOnInit() {

		this.appInfo.isHomeSource.subscribe(
			result => this.isHome = result
		);

		this.appInfo.favList.subscribe(state => {
			if (state.length > 0) {
				this.hasFav = true;
			} else {
				this.hasFav = false;
			}
		});
	}

	open(content) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-header' });
	}

}
