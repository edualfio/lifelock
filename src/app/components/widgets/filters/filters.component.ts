import { Component, Output, OnInit, EventEmitter } from '@angular/core';
import { ProductsService } from '../../../services/products.service';
import { AppInfoService } from '../../../services/app-info.service';
import * as _ from 'lodash';

@Component({
	selector: 'app-filters',
	templateUrl: './filters.component.html',
	styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
	public filtersToShow: any;
	public filters: any;
	@Output() changeFilter = new EventEmitter();

	constructor(
		private appInfo: AppInfoService,
		private productsService: ProductsService
	) {
		this.filtersToShow = this.productsService.getFilters();
		this.retrieveFilters();
	}

	ngOnInit() { }

	retrieveFilters() {
		this.appInfo.filterState.subscribe(state => {
			this.filters = state;
		});
	}

	cleanFilters() {
		this.filters = {};
		this.appInfo.changeFilters(this.filters);
		this.changeFilter.emit(true);
	}

	toggleFilter(type, filter) {

		if (this.filters[type] === filter) {
			delete this.filters[type];
		} else {
			this.filters[type] = filter;
		}

		this.changeFilter.emit(true);
	}

	exist(type, filter) {
		return _.includes(this.filters[type], filter);
	}


}
