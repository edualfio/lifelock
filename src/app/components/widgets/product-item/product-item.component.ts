import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../models/product';
import { AppInfoService } from '../../../services/app-info.service';
import * as _ from 'lodash';

@Component({
	selector: 'app-product-item',
	templateUrl: './product-item.component.html',
	styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

	@Input() product: Product;

	public favourites: any;
	public isInFav = false;

	constructor(
		private appInfo: AppInfoService
	) {


	}

	ngOnInit() {
		this.appInfo.favList.subscribe(state => {
			this.favourites = state;
			this.checkIsFav();
		});
	}

	retrieveFavs() {
		this.appInfo.favList.subscribe(state => {
			this.favourites = state;
		});
	}

	addFavorito(id) {
		this.favourites.push(id);
		this.appInfo.changeFavourites(this.favourites);
		this.checkIsFav();

	}

	delFavorito(id) {
		const index = this.favourites.indexOf(id, 0);
		if (index !== -1) {
			this.favourites.splice(index, 1);
		}

		this.appInfo.changeFavourites(this.favourites);
		this.checkIsFav();

	}

	checkIsFav() {
		this.isInFav = false;
		this.retrieveFavs();
		this.favourites.forEach(id => {
			if (_.isEqual(this.product.id, id)) {
				this.isInFav = true;
			}
		});

	}



}
