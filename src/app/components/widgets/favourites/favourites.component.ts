import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/product';
import { AppInfoService } from '../../../services/app-info.service';
import { ProductsService } from '../../../services/products.service';

@Component({
	selector: 'app-favourites',
	templateUrl: './favourites.component.html',
	styleUrls: ['./favourites.component.scss']
})
export class FavouritesComponent implements OnInit {
	public loading = true;
	public favourites: any;
	public productList: Array<Product>;

	constructor(
		private appInfo: AppInfoService,
		private productsService: ProductsService
	) {
		this.appInfo.favList.subscribe(state => {
			this.favourites = state;
		});

		this.getProducts();
	}

	ngOnInit() {
	}

	getProducts() {
		setTimeout(() => {
			this.productList = this.productsService.getProductById(this.favourites);
			this.loading = false;
		}, 400);
	}

	delFavorito(id) {
		this.loading = true;
		this.removeFromArray(this.favourites, id);
		this.appInfo.changeFavourites(this.favourites);
		this.getProducts();
	}

	removeFromArray(array, el) {
		const index = array.indexOf(el, 0);
		if (index > -1) {
			array.splice(index, 1);
		}
	}

}
