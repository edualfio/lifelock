import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-loading',
	templateUrl: './loading.component.html',
	styles: [
		'.loading {width: 100%; text-align:center;}'
	]


})
export class LoadingComponent implements OnInit {

	constructor() { }

	ngOnInit() {
	}

}
