import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { AppInfoService } from '../../../services/app-info.service';
import { ProductsService } from '../../../services/products.service';

@Component({
	selector: 'app-search-bar',
	templateUrl: './search-bar.component.html',
	styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {
	public step: number;
	public kind: Array<string>;
	public filters: any;
	public loading: boolean;

	constructor(
		private appInfo: AppInfoService,
		private router: Router,
		private productsService: ProductsService
	) {
		this.step = 1;
		this.loading = false;
		this.kind = this.productsService.getFilters().kind;
		this.filters = {};
	}

	@ViewChild('instance') instance: NgbTypeahead;
	focus$ = new Subject<string>();
	click$ = new Subject<string>();

	search = (text$: Observable<string>) => {
		const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
		const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
		const inputFocus$ = this.focus$;

		return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
			map(term => (term === '' ? this.kind
				: this.kind.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
		);
	}

	performSearch() {
		this.loading = true;
		this.appInfo.changeFilters(this.filters);
		this.router.navigate(['list']);

	}

}
