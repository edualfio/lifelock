import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AppInfoService {

	private filtersSource = new BehaviorSubject({});
	filterState = this.filtersSource.asObservable();

	private headerStateSource = new BehaviorSubject(false);
	headerState = this.headerStateSource.asObservable();

	public isHomeSource = new BehaviorSubject(false);
	isHome = this.isHomeSource.asObservable();

	public favSource = new BehaviorSubject([]);
	favList = this.favSource.asObservable();

	constructor() { }

	changeisHome(isHome: boolean) {
		this.isHomeSource.next(isHome);
	}

	changeHeaderState(state: boolean) {
		this.headerStateSource.next(state);
	}

	changeFilters(filters: any) {
		this.filtersSource.next(filters);
	}

	changeFavourites(add: Array<any>) {
		console.log('cambio g');
		this.favSource.next(add);
	}

}
