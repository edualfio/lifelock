import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { products } from '../data/products';
import * as _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class ProductsService {

	constructor() { }

	getProducts(filters, productPerPage, page, sort, order): Array<Product> {
		const response: any = { products: [], total: 0, pages: 0 };
		const result = _.filter(products, filters);
		const ordered = _.orderBy(result, (o) => o[sort], order);
		const final = ordered.slice(page * productPerPage, (page + 1) * productPerPage);
		response.total = result.length;
		response.pages = Math.ceil(result.length / 5);
		response.products = final;

		return response;
	}

	getProductById(ids: Array<number>): Array<Product> {
		const result = [];
		ids.forEach(id => {
			_.map(products, (product) => {
				if (_.isEqual(product.id, id)) {
					result.push(product);
				}
			});
		});

		return result;
	}

	getFilters(): any {
		const filters = {
			brand: [],
			kind: []
		};

		Object.keys(products).forEach(key => {
			const value = products[key];
			filters.brand.push(value.brand);
			filters.kind.push(value.kind);
		});

		filters.brand = filters.brand.filter(this.onlyUnique);
		filters.kind = filters.kind.filter(this.onlyUnique);

		return filters;
	}

	onlyUnique(value, index, self) {
		return self.indexOf(value) === index;
	}
}
