import { Component, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppInfoService } from './services/app-info.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html'
})
export class AppComponent {
	public title = 'lifelock';
	public headerState: boolean;

	constructor(
		private router: Router,
		private appInfo: AppInfoService
	) {
		this.appInfo.headerState.subscribe(state => this.headerState = state);
	}

	@HostListener('window:scroll', ['$event']) onScrollEvent($event) {
		if ($event.pageY > 350) {
			this.headerState = true;
		} else {
			this.headerState = false;
		}
	}

}
