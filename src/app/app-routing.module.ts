import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/pages/home/home.component';
import { ListComponent } from './components/pages/list/list.component';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent
	}, {
		path: 'list',
		component: ListComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
