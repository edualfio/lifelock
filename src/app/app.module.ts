import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ProductsService } from './services/products.service';
import { AppInfoService } from './services/app-info.service';

import { HomeComponent } from './components/pages/home/home.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { SearchBarComponent } from './components/widgets/search-bar/search-bar.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { ListComponent } from './components/pages/list/list.component';
import { ProductItemComponent } from './components/widgets/product-item/product-item.component';
import { FiltersComponent } from './components/widgets/filters/filters.component';
import { FavouritesComponent } from './components/widgets/favourites/favourites.component';
import { LoadingComponent } from './components/widgets/loading/loading.component';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		SearchBarComponent,
		FooterComponent,
		ListComponent,
		ProductItemComponent,
		FiltersComponent,
		FavouritesComponent,
		LoadingComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		NgbModule
	],
	providers: [
		ProductsService,
		AppInfoService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
