# Lifelock

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.2.

## First Step to run Lifelock!

Run `npm install` & `npm start`. That command runs the dev server.

Navigate to `http://localhost:4200/`.


## Further help

### App's Folder structure

Main folder into src folder is `/app` that contains the `app.module` (also the *component* and the *routing* file) and 3 kind of components:

- **`Layout`**: Interface Module. This module is responsible only for managing the main structure of the front.  
- **`Pages`**: SPA deploys two main pages. The Home page has the search bar and info about the company. List page shows the results from the queries.
- **`Widgets`**: Each unit component. These ones can be reused in anothers place of SPA.


### Other considerations

The project is built with the last version of Angular (7.3.2).

All the code follows the best practices proposed by the team of angular for the legibility of the project, as well as the use of Atom Beautifier and a linter of Typescript.

There aren't images assigned to kind of insurances. I prefer to use colors to distinc between types.

As the calls to the dataMock are simulated, so I have added false load times.

I have used Bootstrap 4 as a layout framework, which is responsive.

I use several third-party dependencies: material design icons, lodash. All installed by npm.

You can find a build on my server [lifelock!](http://edualfaro.com/lifelock/)

The project it works on Firefox Chrome (last version of both) and Internet Explorer 9+
